const express  = require("express");
const app = express();
const port = 3000;

// Middlewares

app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.get("/home", (req,res) =>{
	res.send("Welcome to the homepage");
})

let users = [{
	username: "johndoe",
	password: "johndoe1234"
}]

app.get("/users", (req,res) =>{
	res.send(users)
})

/*let users = {
}

app.get("/users", (req,res) =>{
	users = { "username": req.body.username, "password": req.body.password} 
	res.send(users)
})
*/


app.delete("/delete-user", (req, res) => {
	console.log(users)
  let msg = "";
  if (users.length !== 0) {
    for (let i = 0; i < users.length; i++) {
      if (req.body.username === users[i].username) {
        msg = `User ${req.body.username} is has been deleted!`;
        users.splice(i, 1);
        break;
      } else {
        req.body.username === ""
          ? (msg = "Please input username.")
          : (msg = "Username not found!");
      }
    }
    res.send(msg);
  } else res.send("There are no users registered yet!");
});




app.listen(port, () => console.log(`Server running at port ${port}`));

